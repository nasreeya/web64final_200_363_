const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'Train',
    database :'TrainTicketSystem'

})

connection.connect();

const express  = require('express')
const app = express()
const port = 4000


/* CRUD Operation for TrainTicketSystem */
app.get("/list_event" , (req, res) =>{
    let query = "SELECT * from TrainEvent";
   connection.query( query, (err, rows) => {
     if(err){
         res.json({
                    "status" : "400",
                    "message": "Error querying from running db"
                 })
         }else{
             res.json(rows)
          }
       });
})


app.post("/add_event" , (req, res) => {

    let event_origin = req.query.event_origin
    let event_terminal = req.query.event_terminal
 
    let query = ` INSERT INTO TrainEvent 
                   (EventOrigin, EventTerminal)
                   VALUES ('${event_origin}','${event_terminal}' )`
     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             res.json({
                        "status" : "400",
                        "message": "Error inserting data into db"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Adding event succesful"  
              })
           }
      });
 
  })


  app.post("/update_event" , (req, res) => {

    let event_id = req.query.event_id
    let event_origin = req.query.event_origin
    let event_terminal = req.query.event_terminal
 
    let query = `UPDATE TrainEvent SET
                   EventOrigin= '${event_origin}',
                   EventTerminal='${event_terminal}'
                   WHERE EventID=${event_id}`

     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             console.log(err)
             res.json({
                        "status" : "400",
                        "message": "Error updating record"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Updating event succesful"  
              })
           }
      });
 
  })

  app.post("/delete_event" , (req, res) => {

    let event_id = req.query.event_id

    let query = `DELETE FROM TrainEvent WHERE EventID=${event_id}`

     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             console.log(err)
             res.json({
                        "status" : "400",
                        "message": "Error deleting record"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Deleting record success"  
              })
           }
      });
 
  })


app.listen(port, () => {
    console.log(` Now starting Running System Backend ${port} `)
})

/*
query = "SELECT * from User";
connection.query( query, (err, rows) =>{
  if(err){
      console.log(err);
  }else{
      console.log(rows);
  
       }
});
connection.end();*/